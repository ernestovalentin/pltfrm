# Pltfrm
Pltfrm es el desarrollo de un CMS (Sistema Gestor de Contenido) para la implementación de proyectos basados en plataformas web.

# Structure

* Landing Page
* Web Site
* Professional Web
* Marketing Web
* Platform Web
* Web Application
* Windows App
* Android App

# Docs
La Wiki se encuentra en [Gitlab.com](https://gitlab.com/ernestovalent/pltfrm/wikis/home) y la documentación del proyecto se encuentra en: [Gitbook.com](https://ernestovalent.gitbooks.io/cssystem-pantilla-mvc)


# Next Version
* 0.2 Landing Page With Contact Form *- Versión no publicada*

## Changelogs
* Modelos de conexión a base de datos.
  * Flow: Si no existe base de datos, crearla.
  * Flow: Si no existe tabla especificas.
* Crear formulario de contacto.
  * Validar formulario
  * Registrar en Base de datos
  * Posibilidad de mandar Mails
* Crear funcionalidades de Login
  * Crear CRUD de usuarios
* remplazar tabla site en archivo local

<?php

/** Cnx.php
 *  Class of Data Base conexion
 *  Autor: Ernesto Valentin Caamal Peech
 *  2019/05/14
 */
//Link https://victorroblesweb.es/2014/07/15/ejemplo-php-poo-mvc/

class Cnx
{

  private $user, $passwrd, $arrOptions;

  function __construct()
  { //Require NEW Cnx(); where it is used
    $this->user = DBUSER;
    $this->passwrd = DBPASSWRD;
    $this->dsn = "mysql:host=" . DBHOST . ";charset=UTF8";
    $this->arrOptions = array(
      PDO::ATTR_EMULATE_PREPARES => FALSE,
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
      PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8 "
    );
    //$this->dsn = "mysql:host=localhost;dbname=pltfrm;charset=UTF8"; //This conexion use to MySQL
    //$this->dsn = "sqlsrv:server=xcappadd;Database=nitgenacdbsql00"; //This conexion use to SQLServer
  }

  protected function Cnx()
  { //Return PDO Conexion
    $connexion = false;
    try {
      $connexion = new PDO($this->dsn, $this->user, $this->passwrd, $this->arrOptions);
    } catch (PDOException $e) {
      include "view/core/dbError.php"; /* Include page for the best description of data base error*/
      //die("<font size='18'>:´(</font><br><b>Error con la base de datos.</b>"); //die Fishished all code php on one line.
      die();
    } finally {
      return $connexion;
    }
    unset($connexion);
  }
}

<?php
 /** Passwrd.php
  *  Class of functions that use to best password
  *  Autor: Ernesto Valentin Caamal Peech
  *  2019/11/09
  */
 //Link https://diego.com.es/encriptacion-y-contrasenas-en-php
 //Link https://petrimaki.com/2014/09/13/simple-php-crypt-and-mysql-implementation-using-sha256/
 //Link https://www.geeksforgeeks.org/how-to-encrypt-and-decrypt-a-php-string/
 


  class Passwrd{
    private $tbl;
    private $db;
    private $cnx;
    private $dtbs= DBNAME;  //Specific database to use in this class

    function __construct($table){ //Constructor use as: New CnxMdl("tablename");
      $this->tbl=(string) $table; //Save table on private var
      $this->db=New Cnx(); //Save the PDO class conexion on private var
      $this->cnx=$this->db->cnx(); //Save the conexion on private var
      $this->useDataBase(); //Selected Database on this Class
      $this->validateTable($this->tbl); //Validate if table exists
    }

    private function useDataBase(){ //Prepare SQL to use with database specific
      try {
        $sql="USE $this->dtbs";
        $stmt=$this->cnx->exec($sql);
      } catch (PDOException $e) {
        die("No se puede utilizar la base de datos: $this->dtbs.");
      }finally{
        unset($sql);unset($stmt);
      }
    }

    private function validateTable($tbl){ //Validate if table specific works
      try {
        $sql = "SELECT * FROM $tbl LIMIT 1";
        $stmt = $this->cnx->prepare($sql);
      } catch (PDOException $e) {
        die("No se encuentra la tabla: $tbl en el sistema.");
      }finally{
        unset($sql);unset($stmt);
      }
    }

    public function getColumnNames(){ //Get all columns of the table on information schema in database
      try {
        $sql="SELECT COLUMN_NAME, COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='$this->dtbs' AND TABLE_NAME='$this->tbl' ORDER BY ORDINAL_POSITION ASC";
        $stmt=$this->cnx->prepare($sql);
        if ($stmt->execute()) {
          $results=$stmt->fetchAll();
          if (!empty($results)) {
            return $results;
          }else {
            echo "No hay nada en la tabla";
          }
        }
      } catch (PDOException $e) {
        die("Error en la consulta de SQL: $e");
      }finally{
        unset($sql);unset($stmt);
      }
    }

    public function getDataBase(){
      print_r($this->db);
    }

    public function getConexion(){
      $attributes = array(
        "AUTOCOMMIT", "ERRMODE", "CASE", "CLIENT_VERSION", "CONNECTION_STATUS",
        "ORACLE_NULLS", "SERVER_INFO", "SERVER_VERSION");
      foreach ($attributes as $val) {
        echo "PDO_ATTR_$val:  ";
        echo $this->cnx->getAttribute(constant("PDO::ATTR_$val"))."<br>";
      }
    }

    public function getLastId(){ // Esta funcion solo sirve despues de un insert
      try {
        $sql="SELECT MAX(id) AS ultimoID FROM $this->tbl";
        $stmt=$this->cnx->prepare($sql);
        if ($stmt->execute()) {
          $results=$stmt->fetchAll();
          if (!empty($results)) {
            return $results[0][0];
          }else {
            echo "Tabla vacia";
          }
        }
      } catch (PDOException $e) {
        die("Error en la consulta de SQL: $e");
      }finally{
        unset($sql);unset($stmt);
      }
    }

    public function getAllById($id){
      $id = (string) $id;
      try {
        $sql="SELECT * FROM $this->tbl WHERE id=:id";
        $stmt=$this->cnx->prepare($sql);
        $stmt->bindParam(':id',$id, PDO::PARAM_STR);
        if ($stmt->execute()) {
          $results=$stmt->fetchAll();
          if (!empty($results)) {
            return $results;
          }else {
            echo "No existe el id: $id";
          }
        }
      } catch (PDOException $e) {
        die("Ha ocurrido un error de SQL :    $e");
      }finally{
        unset($sql);unset($stmt);
      }
    }

    public function printAnyArray($array){
      echo "<table><tr><th>#</th>";
      foreach ($array[0] as $column => $columnResults) {
        if (!is_numeric($column)) {
          echo "<th>".$column."</th>";
        }
      }
      echo "</tr>";
      foreach ($array as $row => $values) {
        echo "<tr>";
          echo "<td>".($row+1)."</td>";
        for ($f=0; $f < (count($values)/2) ; $f++) {
          echo "<td>".$values[$f]."</td>";
        }
        echo "</tr>";
      }
      echo "</table>";
    }



    /* [...] Adding more functions */


  }
 ?>

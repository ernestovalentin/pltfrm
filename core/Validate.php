<?php
/** Validate.php
 *  Class of functions to validate Data
 *  Autor: Ernesto Valentin Caamal Peech
 *  2019/11/10
 */

 // Link https://larrysteinle.com/2011/02/20/use-regular-expressions-to-detect-sql-code-injection/


class Validate{

  function __construct()  {

  }

  static public function date($value){
    if (preg_match("/^(\d{1,4})[\/|-|.|\S](\d{1,2})[\/|-|.|\S](\d{1,4})$/m",$value)) {  //Required dd/mm/yyyy for validated
      return true;
    }else {
      return false;
    }
  }

  static public function datetime($value){
    if (preg_match("/^(\d{1,4})[\/|-|.\S](\d{1,2})[\/|-|.|\S](\d{1,4})[\T|\s](\d{1,2})[\:](\d{1,2})[\S|:](\d{2})?[\:|\s|.|Z]?(\d{7})?(?:AM)?(?:PM)?$/m",$value)) {
      return true;
    }else {
      return false;
    }
  }

  static public function number($value, int $long){ //To do
    if (strlen($value) <= $long && filter_var($value,FILTER_VALIDATE_INT)) {
      return true;
    }else {
      return false;
    }
  }

  static public function name(string $value, int $long){
    if (strlen($value) <= $long && preg_match("/^[A-zÀ-ú.'& ]*$/",$value)) {
      return true;
    }else {
      return false;
    }
  }

  static public function text(string $value, int $long){
    if (strlen($value) <= $long &&!preg_match("/[\;\*\\\\]|(\x2d{2,})/", $value) ) {
      return true;
    }else {
      return false;
    }
  }

  static public function email(string $value, int $long){
    if (strlen($value) <= $long && filter_var($value, FILTER_VALIDATE_EMAIL)) {
      return true;
    }else {
      return false;
    }
  }

  static public function password(string $value){ //Required lowercase, uppercase, numeric and special character
    if (strlen($value) >= 8  && preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*.])(?=.{8,})/", $value)) {
      return true;
    }else {
      return false;
    }
  }

  static public function html(string $value, int $long){
    if (strlen($value) <= $long) {
      libxml_use_internal_errors(true);
      $dom = New DOMDocument();
      $dom->loadHTML($value);
      return count(libxml_get_errors()) == 0;
      libxml_clear_errors();
      unset($dom);
    }else{
      return false;
    }
  }

  static public function color($value){
    if (preg_match("/^#?([0-9a-f]{6}|[0-9a-f]{3})$/i",$value)) {
      return true;
    }else {
      return false;
    }
  }

  static public function url($value){
    if (filter_var($value, FILTER_VALIDATE_URL)) {
      return true;
    }else {
      return false;
    }
  }

}

 ?>

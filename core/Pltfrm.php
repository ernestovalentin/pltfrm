<?php
/** Pltfrm.php
 *  Is a main Class that use general functions
 * Autor: Ernesto Valentin Caamal Peech
 * 2019/05/14
 */
  require_once "cntrl/NavCntrl.php";//Require a important Class of Navegation by

class Pltfrm{

  public $url = array(); //Array that keep url friendly

  function __construct(){ //function that init pltfrm
    $cnfg = require_once "Cnfg.php";
    define("DBHOST",$cnfg["DBHOST"]);
    define("DBNAME",$cnfg["DBNAME"]);
    define("DBUSER",$cnfg["DBUSER"]);
    define("DBPASSWRD",$cnfg["DBPASSWRD"]);
    define("WEBNAME",$cnfg["WEBNAME"]);
    define("WEBDESCRIPTION",$cnfg["WEBDESCRIPTION"]);
    define("WEBAUTHOR",$cnfg["WEBAUTHOR"]);
    define("WEBKEYWORKS",$cnfg["WEBKEYWORKS"]);
    define("WEBHOSTING",$cnfg["WEBHOSTING"]);

    $this->init();
  }

  private function init(){ //function that call views of the urls friendly. Require .htaccess
    if (isset($_GET["v"])) {
      $url = explode("/", $_GET["v"]);
    }else{
      $url[0] = "index"; //Set default home page
    }
    include NavCntrl::includeView($url[0]);
  }


}

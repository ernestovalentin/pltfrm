<meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
<meta name="description" content="<?php echo WEBDESCRIPTION; ?>">
<meta name="keywords" content="<?php echo WEBKEYWORKS; ?>">
<meta name="author" content="<?php echo WEBAUTHOR; ?>">
<meta name="theme-color" content="#f5f5f5"><!--Color Tab on Chrome, Firefox OS and Opera -->
<meta name="msapplication-navbutton-color" content="#f5f5f5"><!--Color Tab on Windows Phone -->
<meta name="apple-mobile-web-app-status-bar-style" content="#f5f5f5"><!--Color Tab on iOS Safari -->
<title><?php echo WEBNAME; ?></title>

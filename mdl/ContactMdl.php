<?php
/** ContactMdl.php
 *  Class of functions to use database for Contacts
 *  Autor: Ernesto Valentin Caamal Peech
 *  2019/11/10
 */
 require_once "core/Cnxdb.php"; //Need for Work to DataBase
class ContactModel extends Cnxdb{
  protected $cnx;

  function __construct()  {
    $this->cnx = New Cnxdb("contact");
  }

  protected function saveContact($array){
		try {
			$sql = "INSERT INTO 
				contact (reference, email, name, lastname, interesting, subject, message, recipient, reservationname, reservationdate) 
				VALUES (:reference, :email, :name, :lastname, :interesting, :subject, :message, :recipient, :reservationname, :reservationdate)";
			$stmt = $this->cnx->getCnx()->prepare($sql);
			$stmt->bindParam(':reference',$array["reference"], PDO::PARAM_STR);
			$stmt->bindParam(':email',$array["email"], PDO::PARAM_STR);
			$stmt->bindParam(':name', $array["name"], PDO::PARAM_STR);
			$stmt->bindParam(':lastname', $array["lastname"], PDO::PARAM_STR);
			$stmt->bindParam(':interesting', $array["interesting"], PDO::PARAM_STR);
			$stmt->bindParam(':subject', $array["subject"], PDO::PARAM_STR);
			$stmt->bindParam(':message', $array["message"], PDO::PARAM_STR);
			$stmt->bindParam(':recipient', $array["recipient"], PDO::PARAM_STR);
			$stmt->bindParam(':reservationname', $array["reservationname"], PDO::PARAM_STR);
			$stmt->bindParam(':reservationdate', $array["date"], PDO::PARAM_STR);
			if ($stmt->execute()) {
				return true;
			}else{
				return false;
			}
		} catch (PDOException $e) {
			die("<br>Ha ocurrido un error de SQL :    $e");
		}finally{
			unset($sql);unset($stmt);
		}
  }

  protected function getContact($id){

  }

  protected function getColumnNames(){
    return $this->cnx->getStructure();
  }

  protected function printColumnNames(){
    $this->cnx->printArray($this->getColumnNames());
  }
}
